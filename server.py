#!/usr/bin/env python2.7

"""
Columbia's COMS W4111.001 Introduction to Databases
Example Webserver

To run locally:

    python server.py

Go to http://localhost:8111 in your browser.

A debugger such as "pdb" may be helpful for debugging.
Read about it online.
"""
import requests
import datetime
import os

from sqlalchemy import *
from sqlalchemy.pool import NullPool

from flask import Flask, request, render_template, g, redirect, Response, url_for, send_file, session, jsonify
from werkzeug import secure_filename

UPLOAD_FOLDER = 'test/'

tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
app = Flask(__name__, template_folder=tmpl_dir)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

app.secret_key = 'A0Zr98j/3zX R~XHH!jmN]LWX/,?RT'

#
# The following is a dummy URI that does not connect to a valid database. You will need to modify it to connect to your Part 2 database in order to use the data.
#
# XXX: The URI should be in the format of: 
#
#     postgresql://USER:PASSWORD@w4111a.eastus.cloudapp.azure.com/proj1part2
#
# For example, if you had username gravano and password foobar, then the following line would be:
#
#     DATABASEURI = "postgresql://gravano:foobar@w4111a.eastus.cloudapp.azure.com/proj1part2"
#
DATABASEURI = "postgresql://ze2113:0879@w4111vm.eastus.cloudapp.azure.com/w4111"


#
# This line creates a database engine that knows how to connect to the URI above.
#
engine = create_engine(DATABASEURI)

#Global variables to keep track of gid,uid,pid

global_gid = 15
global_uid = 14
global_pid = 17

@app.before_request
def before_request():
  """
  This function is run at the beginning of every web request 
  (every time you enter an address in the web browser).
  We use it to setup a database connection that can be used throughout the request.

  The variable g is globally accessible.
  """
  try:
    g.conn = engine.connect()
  except:
    print "uh oh, problem connecting to database"
    import traceback; traceback.print_exc()
    g.conn = None

@app.teardown_request
def teardown_request(exception):
  """
  At the end of the web request, this makes sure to close the database connection.
  If you don't, the database could run out of memory!
  """
  try:
    g.conn.close()
  except Exception as e:
    pass


# Welcome page. Handles login and create user
@app.route('/')
def index():
  """
  request is a special object that Flask provides to access web request information:

  request.method:   "GET" or "POST"
  request.form:     if the browser submitted a form, this contains the data in the form
  request.args:     dictionary of URL arguments, e.g., {a:1, b:2} for http://localhost?a=1&b=2

  See its API: http://flask.pocoo.org/docs/0.10/api/#incoming-request-data
  """

  return render_template("index.html")

# Handles upload picture
@app.route('/upload_picture/<username>', methods=['POST'])
def upload_picture(username):
  file = request.files['file']

  cursor = g.conn.execute("select u.uid from users u where u.username='"+username+"'")
  temp = []
  for result in cursor:
      d = {'uid':result['uid']}
      temp.append(d)
  uidA = temp[0]
  uid = uidA['uid']

  filename = secure_filename(file.filename)
  file.save(os.path.join(uid+'/', filename))

  global global_pid
  global_pid = global_pid +1
  pid = str(global_pid)

  g.conn.execute("insert into photos values('./"+uid+"/"+filename+"','"+pid+"','"+uid+"')")
   
  return redirect(url_for('homepage',un = username))

# Sends picture files to the browser
@app.route('/<uid>/<url>')
def get_picture(uid,url):
    return send_file(uid+'/'+url)

# Navigation to any users public page
@app.route('/<username>')
def user(username):
    cursor = g.conn.execute("select p.pid, p.url, p.uid from photos p where p.uid = (select u.uid from users u where u.username = '"+username+"')")
    names = []
    for result in cursor:
        d = {'url': "."+result['url'],'owner': result['uid'], 'pid': result['pid']}
        names.append(d)
    for p in names:
        cursor = g.conn.execute("SELECT u.username FROM users u WHERE u.uid ='"+p['owner']+"'")
        for result in cursor:
            p['owner'] = result['username']
    for p in names:
        cursor = g.conn.execute("SELECT COUNT(l.uid) FROM likes l WHERE l.pid='"+p['pid']+"'")
        for result in cursor:
            d = {'likes': result['count']}
            p.update(d)

    context = dict(name = username, photos = names)
    return render_template("user.html",**context)

#Display the group info -> Members and Pictures of the members
@app.route('/group/<gid>')
def get_group(gid):

    cursor = g.conn.execute("SELECT p.uid, p.url, p.pid FROM photos p INNER JOIN users u ON p.uid = u.uid INNER JOIN is_a_member_of i ON i.uid = u.uid WHERE i.gid = '"+gid+"'")
    
    names = []
    for result in cursor:
        d = {'url': "."+result['url'],'owner': result['uid'], 'pid': result['pid']}
        names.append(d)
    for p in names:
        cursor = g.conn.execute("SELECT u.username FROM users u WHERE u.uid ='"+p['owner']+"'")
        for result in cursor:
            p['owner'] = result['username']
    for p in names:
        cursor = g.conn.execute("SELECT COUNT(l.uid) FROM likes l WHERE l.pid='"+p['pid']+"'")
        for result in cursor:
            d = {'likes': result['count']}
            p.update(d)

    
    members = []
    cursor = g.conn.execute("SELECT u.username from users u inner join is_a_member_of i on u.uid = i.uid where i.gid = '"+gid+"'")
        
    for result in cursor:
        d = { 'username':result['username']}
        members.append(d)

    cursor = g.conn.execute("SELECT g.name from groups g where g.gid ='"+gid+"'")

    temp = []
    for result in cursor:
        d = {'name' : result['name']}
        temp.append(d)
    
    context = dict(groups = temp, photos = names, members = members)
    
    return render_template("group.html", **context)
    
# Fecth the comments of a picture
@app.route('/comments/<pid>')
def get_comments(pid):
    cursor = g.conn.execute("select temp.username, temp.text, temp.date from (select us.username, u.uid, u.pid, u.cid, c.text, c.date from (u_comments_p u inner join comments c on u.pid = c.pid and u.cid = c.cid inner join users us on us.uid = u.uid)) as temp where temp.pid = '"+pid+"'")
    c = []
    for result in cursor:
        d = {'commenter': result['username'], 'text': result['text']}
        c.append(d);
    return jsonify(comments = c)

# Search for users 
@app.route('/users/<username>')
def get_users(username):
    cursor = g.conn.execute("select u.username from users u where u.username = '"+username+"'")
    u = []
    for result in cursor:
        d = {'name': result['username']}
        u.append(d);
    return jsonify(users = u)

#Search for tags
@app.route('/tags/<name>')
def get_tags(name):
    cursor = g.conn.execute("select t.name from tags t where t.name = '"+name+"'")
    u = []
    for result in cursor:
        d = {'name': result['name']}
        u.append(d);
    return jsonify(tags = u)

#Display the pictures that were tagged with <name>
@app.route('/tag/<name>')
def display_tag(name):
    cursor = g.conn.execute("select p.pid, p.uid, p.url from photos p where p.pid in (select pt.pid from p_has_t pt where pt.name='"+name+"')")

    names = []
    for result in cursor:
        d = {'url': "."+result['url'],'owner': result['uid'], 'pid': result['pid']}
        names.append(d)
    for p in names:
        cursor = g.conn.execute("SELECT u.username FROM users u WHERE u.uid ='"+p['owner']+"'")
        for result in cursor:
            p['owner'] = result['username']
    for p in names:
        cursor = g.conn.execute("SELECT COUNT(l.uid) FROM likes l WHERE l.pid='"+p['pid']+"'")
        for result in cursor:
            d = {'likes': result['count']}
            p.update(d)

    context = dict(name = name, photos = names)
    
    return render_template("tag.html", **context);

#Search for category
@app.route('/category/<name>')
def display_category(name):
    cursor = g.conn.execute("select p.pid, p.uid, p.url from photos p where p.pid in (select pt.pid from p_has_t pt where pt.name in (select t.name from tags t where t.category = '"+name+"'))")
    
    names = []
    for result in cursor:
        d = {'url': "."+result['url'],'owner': result['uid'], 'pid': result['pid']}
        names.append(d)
    for p in names:
        cursor = g.conn.execute("SELECT u.username FROM users u WHERE u.uid ='"+p['owner']+"'")
        for result in cursor:
            p['owner'] = result['username']
    for p in names:
        cursor = g.conn.execute("SELECT COUNT(l.uid) FROM likes l WHERE l.pid='"+p['pid']+"'")
        for result in cursor:
            d = {'likes': result['count']}
            p.update(d)

    context = dict(name = name, photos = names)

    return render_template("tag.html", **context);

# Handles like request
@app.route('/like/<pid>/<username>')
def like(pid, username):
    cursor = g.conn.execute("select u.uid from users u where u.username = '"+username+"'")
    temp = []
    for result in cursor:
        d = {'uid':result['uid']}
        temp.append(d)
    uidA = temp[0]
    uid = uidA['uid']
    g.conn.execute("insert into likes values ('"+pid+"','"+uid+"')")
    return redirect(url_for('homepage',un = username))

#Create group
@app.route('/create_group')
def create_group():
    groupname = request.args['groupname']
    username = request.args['username']
    
    cursor = g.conn.execute("select u.uid from users u where u.username = '"+username+"'")
    temp = []
    for result in cursor:
        d = {'uid':result['uid']}
        temp.append(d)
    uidA = temp[0]
    uid = uidA['uid']
    
    global global_gid
    global_gid = global_gid + 1
    gid = str(global_gid)
    g.conn.execute("insert into groups values ('"+gid+"','"+groupname+"','"+uid+"')")
    return redirect(url_for('homepage',un = username))

#Add a member to the group
@app.route('/add_member')
def add_member():
    groupname = request.args['groupname']
    username = request.args['username']
    
    cursor = g.conn.execute("select u.uid from users u where u.username = '"+username+"'")
    temp = []
    for result in cursor:
        d = {'uid':result['uid']}
        temp.append(d)
    uidA = temp[0]
    uid = uidA['uid']
    g.conn.execute("insert into is_a_member_of values ('"+groupname+"','"+uid+"')")
    return redirect(url_for('get_group',gid = groupname))

#Handles follow request
@app.route('/follow/<un1>/<un2>')
def follow(un1,un2):
    cursor = g.conn.execute("select u.uid from users u where u.username='"+un1+"'")
    temp = []
    for result in cursor:
        d = {'uid':result['uid']}
        temp.append(d)
    uidA = temp[0]
    uid1 = uidA['uid']

    cursor = g.conn.execute("select u.uid from users u where u.username='"+un2+"'")
    temp = []
    for result in cursor:
        d = {'uid':result['uid']}
        temp.append(d)
    uidA = temp[0]
    uid2 = uidA['uid']

    dt = datetime.date.today()

    dts = str(dt)

    g.conn.execute("insert into follows values ('"+dts+"','"+uid1+"','"+uid2+"')")

    return redirect(url_for('homepage',un = un1))

#Homepage os the logged in user          
@app.route('/homepage/<un>')
def homepage(un):
    if un in session:
        cursor = g.conn.execute("SELECT p.url, p.pid, p.uid FROM photos p WHERE p.uid IN (SELECT f.uid2 FROM follows f WHERE f.uid1 = (SELECT u.uid FROM users u WHERE u.username='"+un+"'))")
        names = []
        for result in cursor:
            d = {'url': "."+result['url'],'owner': result['uid'], 'pid': result['pid']}
            names.append(d)
        for p in names:
            cursor = g.conn.execute("SELECT u.username FROM users u WHERE u.uid ='"+p['owner']+"'")
            for result in cursor:
                p['owner'] = result['username']

        for p in names:
            cursor = g.conn.execute("SELECT COUNT(l.uid) FROM likes l WHERE l.pid='"+p['pid']+"'")
            for result in cursor:
                d = {'likes': result['count']}
                p.update(d);
        
        groups = []
        cursor = g.conn.execute("SELECT g.name, g.gid FROM groups g where g.owner = (select u.uid from users u where u.username = '"+un+"')")


        for result in cursor:
            d = { 'gid' : result['gid'], 'name':result['name']}
            groups.append(d)

        context = dict(name = un, photos = names, groups = groups)

        return render_template("homepage.html",**context)

    return render_template("index.html");

#Handles login request
@app.route('/login')
def login():

    name = request.args['username']
    password = request.args['password']
    cursor = g.conn.execute("SELECT username FROM users WHERE username='"+name+"' AND password='"+password+"'")
    names = []
    for result in cursor:
        names.append(result['username'])
    if(len(names)>0):
        session[name] = 'username'
        return redirect(url_for('homepage',un = name))
    else:
        return redirect('/')

#Create new user
@app.route('/signup')
def signup():
    name = request.args['username']
    password = request.args['password']
    global global_uid
    global_uid = global_uid + 1
    uid = str(global_uid)

    cursor = g.conn.execute("select u.uid from users u where u.username = '"+name+"'")
    temp = []
    for result in cursor:
        d = {'uid':result['uid']}
        temp.append(d)
    if(len(temp)>0):
        return redirect('/')
    else:
        g.conn.execute("insert into users values ('"+name+"','"+uid+"','"+password+"')")
        session[name] = 'username'
        return redirect(url_for('homepage',un = name))



if __name__ == "__main__":
  import click

  @click.command()
  @click.option('--debug', is_flag=True)
  @click.option('--threaded', is_flag=True)
  @click.argument('HOST', default='0.0.0.0')
  @click.argument('PORT', default=8111, type=int)
  def run(debug, threaded, host, port):
    """
    This function handles command line parameters.
    Run the server using:

        python server.py

    Show the help text using:

        python server.py --help

    """

    HOST, PORT = host, port
    print "running on %s:%d" % (HOST, PORT)
    app.run(host=HOST, port=PORT, debug=debug, threaded=threaded)


  run()
